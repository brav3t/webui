﻿using BeadandoPageObjectTest.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoPageObjectTest.Widgets
{
    class ContentWidget : BasePage
    {
        public ContentWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement ContentsTitle => Driver.FindElement(By.Id("toc"));

        public bool ContentsIsVisible()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(d => d.FindElement(By.Id("toc")).Displayed);
            return ContentsTitle.Displayed;
        }
    }
}

