﻿using BeadandoPageObjectTest.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoPageObjectTest.Widgets
{
    class WikipediaWidget : BasePage
    {
        public WikipediaWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public void ClickOnElement(string link)
        {
            Driver.FindElement(By.LinkText(link)).Click();

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(d => d.FindElement(By.Id("firstHeading")).Displayed);
        }

        public ContentWidget GetContentsTitleWidget()
        {
            return new ContentWidget(Driver);
        }

        public FirstHeadingWidget GetFirstHeadingWidget()
        {
            return new FirstHeadingWidget(Driver);
        }
    }
}
