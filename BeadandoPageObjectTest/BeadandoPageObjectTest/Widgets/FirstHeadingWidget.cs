﻿using BeadandoPageObjectTest.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoPageObjectTest.Widgets
{
    class FirstHeadingWidget : BasePage
    {
        public FirstHeadingWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement FirstHeading => Driver.FindElement(By.Id("firstHeading"));

        public bool headerIsVisible()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(d => d.FindElement(By.Id("firstHeading")).Displayed);
            return FirstHeading.Displayed;
        }

    }

}
