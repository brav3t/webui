﻿
using BeadandoPageObjectTest.Pages;
using BeadandoPageObjectTest.Widgets;
using NUnit.Framework;
using System.Collections;
using System.Xml.Linq;

namespace BeadandoPageObjectTest
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("TestData")]
        public void AssignmentTest(string link)
        {
            WikipediaWidget widget = WikipediaPage.Navigate(Driver);

            widget.ClickOnElement(link);

            bool headerIsVisible = widget.GetFirstHeadingWidget().headerIsVisible();
            bool contentsIsVisible = widget.GetContentsTitleWidget().ContentsIsVisible();

            Assert.IsTrue(headerIsVisible);
            Assert.IsTrue(contentsIsVisible);
        }

        static IEnumerable TestData()
        {
            var doc = XElement.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");
            return
                from vars in doc.Descendants("testData")
                let link = vars.Attribute("link").Value
                select new object[] { link };
        }
    }
}
