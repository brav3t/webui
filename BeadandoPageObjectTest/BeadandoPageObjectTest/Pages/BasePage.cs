﻿using OpenQA.Selenium;

namespace BeadandoPageObjectTest.Pages
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

        public virtual string? HeadingText { get; }
    }
}
