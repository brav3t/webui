﻿using BeadandoPageObjectTest.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoPageObjectTest.Pages
{
    class WikipediaPage : BasePage
    {
        public WikipediaPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static WikipediaWidget Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://en.wikipedia.org/wiki/Code_smell";
            return new WikipediaWidget(webDriver);
        }
    }
}
